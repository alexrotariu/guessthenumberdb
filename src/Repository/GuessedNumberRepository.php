<?php

namespace App\Repository;

use App\Entity\GuessedNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GuessedNumber|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuessedNumber|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuessedNumber[]    findAll()
 * @method GuessedNumber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuessedNumberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GuessedNumber::class);
    }

    public function deleteAll()
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'DELETE
            FROM App\Entity\GuessedNumber g'
        );

        $query->execute();
    }
}
