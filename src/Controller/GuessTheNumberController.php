<?php

namespace App\Controller;

use Feedback;
use App\Entity\GuessedNumber;
use App\Repository\GuessedNumberRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GuessTheNumberController extends AbstractController
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/guess/the/number/{feedback}", name="guess_the_number")
     */
    public function index(string $feedback = Feedback::nimic): Response
    {

        $last_guess = null;
        $current_guess = null;
        if ($this->session->get('lower') != null) {
            $lower_limit = $this->session->get('lower');
        } else {
            $lower_limit = 0;
        }
        if ($this->session->get('upper') != null) {
            $upper_limit = $this->session->get('upper');
        } else {
            $upper_limit = 100;
        }
        $message = "I guess";

        switch ($feedback) {
            case Feedback::nimic:
                $this->clearSavedData();
                $this->session->set('lower', 0);
                $this->session->set('upper', 100);
                $lower_limit = $this->session->get('lower');
                $upper_limit = $this->session->get('upper');
                $current_guess = rand($lower_limit, $upper_limit);
                $this->saveGuess($current_guess);
                break;
            case Feedback::prea_mare:
                $last_guess = $this->getLastGuess();
                $upper_limit = $last_guess->getNumber() - 1;
                $this->session->set('upper', $upper_limit);
                $current_guess = rand($lower_limit, $upper_limit);
                $this->saveGuess($current_guess);
                break;
            case Feedback::prea_mic:
                $last_guess = $this->getLastGuess();
                $lower_limit = $last_guess->getNumber() + 1;
                $this->session->set('lower', $lower_limit);
                $current_guess = rand($lower_limit, $upper_limit);
                $this->saveGuess($current_guess);
                break;
            case Feedback::exact:
                $last_guess = $this->getLastGuess();
                $current_guess = $last_guess->getNumber();
                $message = "Cool! I found it:";
                break;
        }

        return $this->render('guess_the_number/index.html.twig', [
            'current_guess' => $current_guess,
            'lower_limit' => $lower_limit,
            'upper_limit' => $upper_limit,
            'message' => $message,
            'lower' => $lower_limit,
            'upper' => $upper_limit
        ]);
    }

    private function saveGuess($number)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $guessedNumber = new GuessedNumber();
        $guessedNumber->setNumber($number);

        $entityManager->persist($guessedNumber);

        $entityManager->flush();
    }

    private function clearSavedData()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(GuessedNumber::class);

        $repository->deleteAll();
    }

    public function getLastGuess(): GuessedNumber
    {

        $repository = $this->getDoctrine()->getManager()->getRepository(GuessedNumber::class);

        $results = $repository->findBy(array(), array('id' => 'DESC'), 1, 0);
        return $results[0];
    }
}
